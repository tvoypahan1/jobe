package com.timur.kozhurkin.pet.projects.restaurants.helper.model;

public class User {
	private String name;
	private String surname;
	private String phoneNumber;
	private int age;
	
	private User() {}
	//
	private User(String name, String surname, String phoneNumber, int age) {
		this.name = name;
		this.surname = surname;
		this.phoneNumber = phoneNumber;
		this.age = age;
	}
	
	public static Builder builder() {
		
		return new Builder();
	}
	
	static class Builder {
		private String name;
		private String surname;
		private String phoneNumber;
		private int age;
		
		public Builder() {}
		
		Builder name(String name) {
			this.name = name;
			return this;
		}
		
		Builder surname(String surname) {
			this.surname = surname;
			return this;
		}
		
		Builder phoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}
		
		Builder age(int age) {
			this.age = age;
			return this;
		}
		
		User build() {
			return new User(name, surname, phoneNumber, age);
		}

		
	}
}
