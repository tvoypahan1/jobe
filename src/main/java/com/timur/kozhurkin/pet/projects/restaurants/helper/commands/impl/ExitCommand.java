package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import java.rmi.RemoteException;

import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;

public class ExitCommand implements Command {
	//
	@Override
	public void execute() {
		System.out.println("Работа закончена ");
		System.exit(0);
	}
}
