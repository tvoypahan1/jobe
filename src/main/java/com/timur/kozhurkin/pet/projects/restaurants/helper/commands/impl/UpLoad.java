package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.rmi.RemoteException;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;

public class UpLoad implements Command {
	
	@Override	
	public void execute() {
		try {
			FileInputStream fis = new FileInputStream("storage.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			InMemoryStorage inMemoryStorage = (InMemoryStorage) ois.readObject();
			
			System.out.println(inMemoryStorage);
			
			ois.close();
		} 	catch (IOException e) {
				System.out.println("Не работает");
				e.printStackTrace();
		} 	catch (ClassNotFoundException e) {
				System.out.println("Не работает");
				e.printStackTrace();
		}
	}

}
	
