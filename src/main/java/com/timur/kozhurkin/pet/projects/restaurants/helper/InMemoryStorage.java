package com.timur.kozhurkin.pet.projects.restaurants.helper;

import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.CreateItemCommand;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.ExitCommand;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.ShowItemsCommand;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InMemoryStorage implements Serializable{
	private static volatile InMemoryStorage inMemoryStorage = null;
	
	private InMemoryStorage() {}
	
	public static InMemoryStorage getInMemoryStorage() {
		if(inMemoryStorage == null) {
			synchronized(InMemoryStorage.class) {
				if(inMemoryStorage == null) {
					inMemoryStorage = new InMemoryStorage();
				}
			}
		}
		return inMemoryStorage;
	}
	
    private List<Admin> listAdminItems = new ArrayList<>();
    private List<Command> listCommandItems = new ArrayList<>();
    private List<CreateItemCommand> listCreateItemCommandItems = new ArrayList<>();
    private List<ExitCommand> listExitCommandItems = new ArrayList<>();
    private List<ItemGroup> listItemGroupItems = new ArrayList<>();
    private List<Menu> listMenuItems = new ArrayList<>();
    private List<MenuItem> listMenuItemItems = new ArrayList<>();
    private List<Order> listOrderItems = new ArrayList<>();
    private List<Role> listRoleItems = new ArrayList<>();
    private List<ShowItemsCommand> listShowItemsCommandItems = new ArrayList<>();
    private List<Table> listTableItems = new ArrayList<>();
    private List<User> listUserItems = new ArrayList<>();
    
    public List<Admin> getListAdminItems() {
    	return listAdminItems;
    }
    
    public List<Command> getListCommandItems() {
    	return listCommandItems;
    }

    public List<CreateItemCommand> getListCreateItemCommandItems() {
    	return listCreateItemCommandItems;
    }
    
    public List<ExitCommand> getListExitCommandItems() {
    	return listExitCommandItems;
    }
    
    public List<ItemGroup> getListItemGroupItems() {
    	return listItemGroupItems;
    }
    
    public List<Menu> getListMenuItems() {
    	return listMenuItems;
    }
    
    public List<MenuItem> getListMenuItemItems() {
    	return listMenuItemItems;
    }
    
    public List<Order> getListOrderItems() {
    	return listOrderItems;
    }
    
    public List<Role> getListRoleItems() {
    	return listRoleItems;
    }
    
    public List<ShowItemsCommand> getListShowItemsCommandItems() {
    	return listShowItemsCommandItems;
    }
    
    public List<Table> getListTableItems() {
    	return listTableItems;
    }
    
    public List<User> getListUserItems() {
    	return listUserItems;
    }
    //
}

