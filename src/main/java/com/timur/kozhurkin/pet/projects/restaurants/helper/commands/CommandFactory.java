package com.timur.kozhurkin.pet.projects.restaurants.helper.commands;

import java.rmi.RemoteException;

import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.*;

public class CommandFactory {
	 CreateItemCommand createItemCommand = new CreateItemCommand();
	 ShowItemsCommand showItemsCommand = new ShowItemsCommand();
	 
	 CreateTablesCommand createTablesCommand = new CreateTablesCommand();
	 ShowTablesCommand showTablesCommand = new ShowTablesCommand();
	 
	 CreateOrderCommand createOrderCommand = new CreateOrderCommand();
	 ShowOrderCommand showOrderCommand = new ShowOrderCommand();
	 DeleteOrder deleteOrder = new DeleteOrder();
	 
	 DownLoad downLoad = new DownLoad();
	 UpLoad upLoad = new UpLoad();
	 ExitCommand exitCommand = new ExitCommand();
	 HelpCommand helpCommand = new HelpCommand();
	 //
	public void create(String typeOfCommand) throws RemoteException {
			switch (typeOfCommand) {
				case "CreateMenuItems" : createItemCommand.execute();
				break;
				case "com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.DownLoad" : downLoad.execute();
				break;
				case "com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.UpLoad" : upLoad.execute();
				break;
				case "ShowMenuItems" : showItemsCommand.execute();
				break;
				case "CreateTables" : createTablesCommand.execute();
				break;
				case "ShowTables" : showTablesCommand.execute();
				break;
				case "CreateOrder" : createOrderCommand.execute();
				break;
				case "ShowOrder" : showOrderCommand.execute();
				break;
				case "DeleteOrder" : deleteOrder.execute();
				break;
				case "Exit" : exitCommand.execute();
				break;
				case "Help" : helpCommand.execute();
				break;
			}
	}
}
