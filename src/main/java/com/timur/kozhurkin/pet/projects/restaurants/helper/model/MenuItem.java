package com.timur.kozhurkin.pet.projects.restaurants.helper.model;

import java.io.Serializable;

public class MenuItem implements Serializable {
    @Override
    public String toString() {
        return "com.timur.kozhurkin.pet.projects.restaurants.helper.model.MenuItem [title=" + title + ", description=" + description + ", structure=" + structure + ", price="
                + price + "]";
    }

    private String title;
    private String description;
    private String structure;
    private transient int price;

    private MenuItem() {
    }

    //
    private MenuItem(String title, String description, String structure, int price) {
        this.title = title;
        this.description = description;
        this.structure = structure;
        this.price = price;
    }

    public static Builder builder() {

        return new Builder();
    }

    public static class Builder {
        private String title;
        private String description;
        private String structure;
        private int price;

        public Builder() {
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder structure(String structure) {
            this.structure = structure;
            return this;
        }

        public Builder price(int price) {
            this.price = price;
            return this;
        }

        public Builder allPoints(String title, String description, String structure, int price) {
            this.title = title;

            this.description = description;

            this.structure = structure;

            this.price = price;
            return this;
        }

        public MenuItem build() {
            return new MenuItem(title, description, structure, price);
        }
    }

}
