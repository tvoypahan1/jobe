package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import java.rmi.RemoteException;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;

public class ShowItemsCommand implements Command {
	//
	
	@Override
	public void execute() {
		InMemoryStorage.getInMemoryStorage().getListMenuItemItems().forEach(System.out::println);
	}
}
