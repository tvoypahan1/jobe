package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.util.Scanner;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.CommandRMI;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.MenuItem;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.Order;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.Table;

public class CommandsRMI implements CommandRMI {
	
	@Override
	public void createItemCommand() throws RemoteException {
		String title;
		String description;
		String structure;
		int price;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("1. Введите название. 2. Введите описание. 3. Введите состав. 4. Введите цену.");
		title = input.next();
		description = input.next();
		structure = input.next();
		price = input.nextInt();
		
		MenuItem nameItem = MenuItem.builder().allPoints(title, description, structure, price).build();
		InMemoryStorage.getInMemoryStorage().getListMenuItemItems().add(nameItem);

		System.out.println("Объект создался! ");		
	}

	@Override
	public void createOrderCommand() throws RemoteException {
		Scanner input = new Scanner(System.in);
		
		int numberTable;
		int dish;	
		
		System.out.println("Выберите столик");
		InMemoryStorage.getInMemoryStorage().getListTableItems().forEach(System.out::println);
		numberTable = input.nextInt();
		numberTable--;
		Table table = InMemoryStorage.getInMemoryStorage().getListTableItems().get(numberTable);
		
		System.out.println("Выберите блюдо");
		InMemoryStorage.getInMemoryStorage().getListMenuItemItems().forEach(System.out::println);
		dish = input.nextInt();
		dish--;
		MenuItem menuItem = InMemoryStorage.getInMemoryStorage().getListMenuItemItems().get(dish);
		
		Order order = Order.builder().allPoints(table, menuItem).build();
		InMemoryStorage.getInMemoryStorage().getListOrderItems().add(order);
	}

	@Override
	public void createTablesCommand() throws RemoteException {
		int numberTable;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("1. Введите номер стола");
		
		numberTable = input.nextInt();
		
		Table nameTable = Table.builder().numberTable(numberTable).build();
		InMemoryStorage.getInMemoryStorage().getListTableItems().add(nameTable);

		System.out.println("Объект создался! ");
	}

	@Override
	public void deleteOrder() throws RemoteException {
		Scanner input = new Scanner(System.in);
		int deleteOrder;
		
		System.out.println("Выберите какой заказ убрать");
		InMemoryStorage.getInMemoryStorage().getListOrderItems().forEach(System.out::println);
		deleteOrder = input.nextInt();
		deleteOrder--;
		InMemoryStorage.getInMemoryStorage().getListOrderItems().remove(deleteOrder);
	}

	@Override
	public void downLoad() throws RemoteException {
		 InMemoryStorage singleton = InMemoryStorage.getInMemoryStorage();	
		 
		 try {
	    	   FileOutputStream outputStream = new FileOutputStream("storage.bin");
	    	   ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
	    	   
	    	   objectOutputStream.writeObject(singleton);
	    	   objectOutputStream.close();
	       }   catch (IOException e) {
	    	   		// TODO Auto-generated catch block
	    	   		e.printStackTrace();
	       	   }
	}

	@Override
	public void exitCommand() throws RemoteException {
		System.out.println("Работа закончена ");
		System.exit(0);		
	}

	@Override
	public void helpCommand() throws RemoteException {
		System.out.println(" Созать меню ");
		System.out.println(" Показать меню \n ");
		
		System.out.println(" Создать номер стола ");
		System.out.println(" Показать столы \n");
		
		System.out.println(" Создать заказ ");
		System.out.println(" Показать заказ \n");
		
		System.out.println(" Скачать ");
		System.out.println(" Загружать ");
		System.out.println(" Показать списки комманд ");
		System.out.println(" Выйти");		
	}

	@Override
	public void showItemsCommand() throws RemoteException {
		InMemoryStorage.getInMemoryStorage().getListMenuItemItems().forEach(System.out::println);
		System.out.println(" Это RMI!");
	}

	@Override
	public void showOrderCommand() throws RemoteException {
		InMemoryStorage.getInMemoryStorage().getListOrderItems().forEach(System.out::println);
		System.out.println(" Это RMI!");

	}

	@Override
	public void showTablesCommand() throws RemoteException {
		InMemoryStorage.getInMemoryStorage().getListTableItems().forEach(System.out::println);
		System.out.println(" Это RMI!");

	}

	@Override
	public void upLoad() throws RemoteException {
		try {
			FileInputStream fis = new FileInputStream("storage.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			InMemoryStorage inMemoryStorage = (InMemoryStorage) ois.readObject();
			
			System.out.println(inMemoryStorage);
			
			ois.close();
		} 	catch (IOException e) {
				System.out.println("Не работает");
				e.printStackTrace();
		} 	catch (ClassNotFoundException e) {
				System.out.println("Не работает");
				e.printStackTrace();
		}		
	}

}
