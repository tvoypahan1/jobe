package com.timur.kozhurkin.pet.projects.restaurants.helper.model;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;

public class Order {
	@Override
	public String toString() {
		return "com.timur.kozhurkin.pet.projects.restaurants.helper.model.Order [table=" + table + ", dish=" + dish + "]";
	}
	
	private Table table;
	private MenuItem dish;
	
	private Order(){}
	
	private Order(Table table, MenuItem dish) {
		this.table = table;
		this.dish = dish;
	}

	
	//Здесь делаю фиксацию старой цены

	
	Status status;
	enum Status{
		READY,
		NOT_YET,
		PAID,
		PREPARE,
		
		// toDo описания заказов: оплаченно, на кухне и тд
	}
	
	
	
	public static Builder builder() {
		
		return new Builder();
	}

	public static class Builder {
		private Table table;
		private MenuItem dish;
		
		public Builder() {}

		public Builder allPoints(Table table, MenuItem dish) {
			this.table = table;
			this.dish = dish;
			return this;
		}

		public Order build() {
			return new Order(table, dish);
		}
	}
	
}


