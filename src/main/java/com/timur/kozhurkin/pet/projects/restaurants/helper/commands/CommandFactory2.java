package com.timur.kozhurkin.pet.projects.restaurants.helper.commands;

import java.rmi.RemoteException;

public class CommandFactory2 {
	
	public void create(String typeOfCommand, CommandRMI commandRMI) throws RemoteException {
		switch (typeOfCommand) {
			case "CreateMenuItems" : commandRMI.createItemCommand();
			break;
			case "com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.DownLoad" : commandRMI.downLoad();
			break;
			case "com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.UpLoad" : commandRMI.upLoad();
			break;
			case "ShowMenuItems" : commandRMI.showItemsCommand();
			break;
			case "CreateTables" : commandRMI.createTablesCommand();
			break;
			case "ShowTables" : commandRMI.showTablesCommand();
			break;
			case "CreateOrder" : commandRMI.createOrderCommand();
			break;
			case "ShowOrder" : commandRMI.showOrderCommand();
			break;
			case "DeleteOrder" : commandRMI.deleteOrder();
			break;
			case "Exit" : commandRMI.exitCommand();
			break;
			case "Help" : commandRMI.helpCommand();
			break;
		}
}
}
