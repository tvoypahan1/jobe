package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import com.timur.kozhurkin.pet.projects.restaurants.helper.*;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.MenuItem;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.Order;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.Table;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.util.Scanner;

public class CreateOrderCommand implements Command {
	
	Scanner input = new Scanner(System.in);
	
	int numberTable;
	int dish;
	
	@Override
	public void execute() {
		System.out.println("Выберите столик");
		InMemoryStorage.getInMemoryStorage().getListTableItems().forEach(System.out::println);
		numberTable = input.nextInt();
		numberTable--;
		Table table = InMemoryStorage.getInMemoryStorage().getListTableItems().get(numberTable);
		
		System.out.println("Выберите блюдо");
		InMemoryStorage.getInMemoryStorage().getListMenuItemItems().forEach(System.out::println);
		dish = input.nextInt();
		dish--;
		MenuItem menuItem = InMemoryStorage.getInMemoryStorage().getListMenuItemItems().get(dish);
		
		Order order = Order.builder().allPoints(table, menuItem).build();
		InMemoryStorage.getInMemoryStorage().getListOrderItems().add(order);
	}
}
