package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import java.util.Scanner;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;

public class DeleteOrder implements Command {
	Scanner input = new Scanner(System.in);
	int deleteOrder;
	
	@Override
	public void execute() {
		System.out.println("Выберите какой заказ убрать");
		InMemoryStorage.getInMemoryStorage().getListOrderItems().forEach(System.out::println);
		deleteOrder = input.nextInt();
		deleteOrder--;
		InMemoryStorage.getInMemoryStorage().getListOrderItems().remove(deleteOrder);
	}
	
}
