package com.timur.kozhurkin.pet.projects.restaurants.helper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
	public static void main(String[] args) throws InstantiationException, IllegalAccessException,
    NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {

		CustomClassLoaderDemo loader = new CustomClassLoaderDemo();
		Class<?> c = loader.findClass("com.timur.kozhurkin.pet.projects.restaurants.helper.ServerMain");
		Object ob = c.newInstance();
		Method md = c.getMethod("mainServer");
		md.invoke(ob);
	}
	//
}
