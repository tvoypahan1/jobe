package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import java.rmi.RemoteException;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;

public class ShowTablesCommand implements Command {
	//
	@Override
	public void execute() {
		InMemoryStorage.getInMemoryStorage().getListTableItems().forEach(System.out::println);
	}
}
