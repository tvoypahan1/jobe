package com.timur.kozhurkin.pet.projects.restaurants.helper;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.CommandFactory;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.CommandFactory2;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.CommandRMI;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.CommandsRMI;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.HelpCommand;

public class ClientMain {
	
	public static final String UNIQUE_BINDING_NAME = "server.restaurants";

	public static void main(String[] args) throws RemoteException, NotBoundException {

		final Registry registry = LocateRegistry.getRegistry(2732);

	    CommandRMI commandRMI = (CommandRMI) registry.lookup(UNIQUE_BINDING_NAME);

	    Scanner input = new Scanner(System.in);
		new CommandsRMI().helpCommand();
		CommandFactory2 cf = new CommandFactory2();

		while (true) {
			String commandCode = input.next();
			cf.create(commandCode, commandRMI);

		}
	   }
}
