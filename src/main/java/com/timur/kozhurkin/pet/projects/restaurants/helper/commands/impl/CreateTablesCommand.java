package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.Table;

import java.rmi.RemoteException;
import java.util.Scanner;

public class CreateTablesCommand implements Command {
	int numberTable;
	//
	@Override
	public void execute() {
		Scanner input = new Scanner(System.in);
		
		System.out.println("1. Введите номер стола");
		
		numberTable = input.nextInt();
		
		Table nameTable = Table.builder().numberTable(numberTable).build();
		InMemoryStorage.getInMemoryStorage().getListTableItems().add(nameTable);

		System.out.println("Объект создался! ");
	}
}
