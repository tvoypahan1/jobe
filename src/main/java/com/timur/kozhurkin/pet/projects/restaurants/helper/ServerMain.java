package com.timur.kozhurkin.pet.projects.restaurants.helper;

import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl.CommandsRMI;

public class ServerMain {
	 public static final String UNIQUE_BINDING_NAME = "server.restaurants";

	   public static void mainServer() throws RemoteException, AlreadyBoundException, InterruptedException {
		   System.out.println("Это RMI");
	       final CommandsRMI server = new CommandsRMI();

	       final Registry registry = LocateRegistry.createRegistry(2732);

	       Remote stub = UnicastRemoteObject.exportObject(server, 0);
	       registry.bind(UNIQUE_BINDING_NAME, stub);

	       Thread.sleep(Integer.MAX_VALUE);

	   }
}
