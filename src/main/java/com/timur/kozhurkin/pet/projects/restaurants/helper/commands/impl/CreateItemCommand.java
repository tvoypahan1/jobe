package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;
import com.timur.kozhurkin.pet.projects.restaurants.helper.model.MenuItem;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Scanner;

public class CreateItemCommand implements Command {
	public File file = new File("somefile.json");
	String title;
	String description;
	String structure;
	int price;
	//
	
	@Override
	public void execute() {
		Scanner input = new Scanner(System.in);
		
		System.out.println("1. Введите название. 2. Введите описание. 3. Введите состав. 4. Введите цену.");
		title = input.next();
		description = input.next();
		structure = input.next();
		price = input.nextInt();
		
		MenuItem nameItem = MenuItem.builder().allPoints(title, description, structure, price).build();
		InMemoryStorage.getInMemoryStorage().getListMenuItemItems().add(nameItem);

		System.out.println("Объект создался! ");
	}
}
