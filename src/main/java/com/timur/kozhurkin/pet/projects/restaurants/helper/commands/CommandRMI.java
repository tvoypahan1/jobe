package com.timur.kozhurkin.pet.projects.restaurants.helper.commands;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CommandRMI extends Remote {
	
	public void createItemCommand() throws RemoteException;
	
	public void createOrderCommand() throws RemoteException;
	
	public void createTablesCommand() throws RemoteException;
	
	public void deleteOrder() throws RemoteException;
	
	public void downLoad() throws RemoteException;
	
	public void exitCommand() throws RemoteException;
	
	public void helpCommand() throws RemoteException;
	
	public void showItemsCommand() throws RemoteException;
	
	public void showOrderCommand() throws RemoteException;
	
	public void showTablesCommand() throws RemoteException;
	
	public void upLoad() throws RemoteException;
}
