package com.timur.kozhurkin.pet.projects.restaurants.helper.commands.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;

import com.timur.kozhurkin.pet.projects.restaurants.helper.InMemoryStorage;
import com.timur.kozhurkin.pet.projects.restaurants.helper.commands.Command;

public class DownLoad implements Command {
	 private InMemoryStorage singleton = InMemoryStorage.getInMemoryStorage();
	 
	 @Override
	 public void execute() {
	       try {
	    	   FileOutputStream outputStream = new FileOutputStream("storage.bin");
	    	   ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
	    	   
	    	   objectOutputStream.writeObject(singleton);
	    	   objectOutputStream.close();
	       }   catch (IOException e) {
	    	   		// TODO Auto-generated catch block
	    	   		e.printStackTrace();
	       	   }
	       
	}
	
}
