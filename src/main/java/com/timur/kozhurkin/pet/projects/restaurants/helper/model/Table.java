package com.timur.kozhurkin.pet.projects.restaurants.helper.model;

import java.io.Serializable;

public class Table implements Serializable {
	//private int sitCount;
	@Override
	public String toString() {
		return "com.timur.kozhurkin.pet.projects.restaurants.helper.model.Table [numberTable=" + numberTable + "]";
	}
	//
	private int numberTable;
	
	private Table(){}
	
	private Table(int numberTable) {
		this.numberTable = numberTable;
	}
	
	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
	    private int numberTable;
		
		public Builder() {}

		public Builder numberTable(int numberTable) {
			this.numberTable = numberTable;
			return this;
		}

		public Table build() {
			return new Table(numberTable);
		}
	}
	
}
