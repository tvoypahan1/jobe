package com.timur.kozhurkin.pet.projects.restaurants.helper.commands;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Command extends Remote {
	public void execute() throws RemoteException;
	//
}
